import re
import time
import datetime
import argparse

def parse_timestring(timestring):
    fp = '[0-9]*\.?[0-9]+'
    wm = '((' + fp + ')w)?'
    dm = '((' + fp + ')d)?'
    hm = '((' + fp + ')h)?'
    mm = '((' + fp + ')m)?'
    sm = '((' + fp + ')s)?'

    ws = '\s*'
    full = wm + ws + dm + ws + hm + ws + mm + ws + sm

    m = re.fullmatch(full, timestring)
    if m == None:
        return None
    else:
        values = list(map(lambda x: 0.0 if x == None else float(x), m.groups()[1::2]))
        return datetime.timedelta(weeks=values[0],days=values[1],hours=values[2],minutes=values[3],seconds=values[4])



parser = argparse.ArgumentParser(description='Interval Executor and Timer')
parser.add_argument('instance_id', metavar='instance id string', help='EC2 instance id')
parser.add_argument('--timeout', metavar='timestring', help='operation timeout')
parser.add_argument('--interval', metavar='timestring', help='operation update interval')

args = parser.parse_args()
if args.timeout == None or args.interval == None:
    parser.error('You must specify timeout and interval')


timeout = parse_timestring(args.timeout)
interval = parse_timestring(args.interval)
instance_id = args.instance_id

if timeout == None or interval == None:
    parser.error('You must specify timeout and interval')

print('Looking for "{}" every {} for {}'.format(instance_id, interval, timeout))
success = False
start_time = datetime.datetime.now()

while True:
    print('{}: request some data and evaluate a condition...'.format(str(datetime.datetime.now())))
    if success:
        break
    if datetime.datetime.now() >= start_time + timeout:
        break
    time.sleep(interval.total_seconds())

if success:
    print('{}: condition met!'.format(str(datetime.datetime.now())))
else:
    print('{}: condition not met!'.format(str(datetime.datetime.now())))
